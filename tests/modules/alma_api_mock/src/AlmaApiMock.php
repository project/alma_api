<?php

namespace Drupal\alma_api_mock;

use Drupal\alma_api\AlmaApi;
use Drupal\alma_api\AlmaApiInterface;

/**
 * Class Alma wrapper for the almas REST endpoint.
 *
 * @package Drupal\alma
 */
class AlmaApiMock extends AlmaApi implements AlmaApiInterface {

  /**
   * {@inheritDoc}
   */
  protected function makeApiCall($library_code, $endpoint, $parameter_string) {
    switch ($library_code) {
      case 'LIB1':
        $hour = (object) [
          'from' => '08:30',
          'to' => '17:30',
        ];
        $library_hours = $this->getRangedLibraryHours($parameter_string, $hour);
        break;

      case 'LIB2':
        $hour = (object) [
          'from' => '09:30',
          'to' => '18:30',
        ];
        $library_hours = $this->getRangedLibraryHours($parameter_string, $hour);
        break;

      case 'LIB3':
        $hour = (object) [
          'from' => '10:30',
          'to' => '19:30',
        ];
        $library_hours = $this->getRangedLibraryHours($parameter_string, $hour);
        break;

      case 'LIB4':
        $hour = (object) [
          'from' => '11:30',
          'to' => '20:30',
        ];
        $library_hours = $this->getRangedLibraryHours($parameter_string, $hour);
        break;

      case 'LIB5':
        $hour = (object) [
          'from' => '12:30',
          'to' => '21:30',
        ];
        $library_hours = $this->getRangedLibraryHours($parameter_string, $hour);
        break;

      default:
        $library_hours = $this->getRangedLibraryHours($parameter_string, (object) [
          'from' => '08:30',
          'to' => '17:30',
        ]);

    }

    if ($library_hours) {
      $library_hours = json_encode($library_hours);
    }

    return $library_hours;
  }

  /**
   * {@inheritDoc}
   */
  public function setLibraries(array $libraries) {

    if (empty($libraries)) {
      $this->libraries = [
        'LIB1' => 'Library 1',
        'LIB2' => 'Library 2',
        'LIB3' => 'Library 3',
        'LIB4' => 'Library 4',
        'LIB5' => 'Library 5',
      ];
    }
    else {
      $this->libraries = $libraries;
    }

  }

  /**
   * @param array $parameter_string
   *
   * @param object $hour
   *
   * @return array|false|string
   *   json_encoded string, replicates alma api.
   * @throws \Exception
   */
  private function getRangedLibraryHours(array $parameter_string, object $hour) {
    $period = $this->getDatePeriodFromTo($parameter_string);

    $library_hours = ['day' => []];

    $count = 0;
    foreach ($period as $dt) {
      $count++;
      // There was a bug where the second day was not displayed in the calendar.
      // So need to ensure that the mock data has 2 days not specified
      // consecutively so we can test for it.
      // Skip every 4th and 5th day so that two days in a row are not
      // defined in the returned data set.
      if ($count == 4 || $count == 5) {
        if ($count == 5) {
          $count = 0;
        }
        continue;
      }

      $library_hours['day'][] = (object) [
        'date' => $dt->format("Y-m-d") . 'Z',
        'day_of_week' => (object) [
          'value' => $dt->format("N"),
          'description' => $dt->format("l"),
        ],
        'hour' => [$hour],
      ];
    }

    return $library_hours;
  }

  /**
   * Return a date period that we can loop over.
   *
   * @param array $parameter_string
   *   An array indexed with to and from dates.
   *
   * @return \DatePeriod
   *   A date period give the to and from strings.
   *
   * @throws \Exception
   */
  private function getDatePeriodFromTo(array $parameter_string): \DatePeriod {
    $begin = new \DateTime($parameter_string['from']);
    $end = new \DateTime($parameter_string['to']);
    $end->modify('+1 day');
    $interval = \DateInterval::createFromDateString('1 day');
    return new \DatePeriod($begin, $interval, $end);
  }

}
