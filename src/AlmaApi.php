<?php

namespace Drupal\alma_api;

use Drupal\Core\Config\ConfigFactoryInterface;

use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManager;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Alma wrapper for the almas REST endpoint.
 *
 * @package Drupal\alma
 */
class AlmaApi implements AlmaApiInterface {

  use ConfigFormBaseTrait;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var array
   *
   * Example:
   * @code
   *   protected $libraries = [
   *     'LIB1' => 'Library 1',
   *     'LIB2' => 'Library 2',
   *     'LIB3' => 'Library 3',
   *     'LIB4' => 'Library 4',
   *     'LIB5' => 'Library 5',
   *   ];
   * @endcode
   */
  protected $libraries;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * @var \Drupal\Core\Mail\MailManager
   */
  private $mailManager;

  /**
   * @var \Drupal\Core\Language\LanguageManager
   */
  private $languageManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $http_client, LoggerChannelFactoryInterface $loggerFactory, MailManager $mailManager, LanguageManager $languageManager) {
    $this->configFactory = $configFactory;
    $this->httpClient = $http_client;
    $this->logger = $loggerFactory->get('alma_api');
    $this->mailManager = $mailManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritDoc}
   */
  public function setLibraries(array $libraries) {
    $this->libraries = $libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraryHours(string $from, string $to) {

    $hours = [];

    foreach ($this->libraries as $library_code => $library_label) {
      $library_code = str_replace(' ', '%20', $library_code);
      $library_hours = $this->makeApiCall($library_code, 'open-hours', ['from' => $from, 'to' => $to]);

      if ($library_hours) {
        $library_hours = $this->hoursFromJsonToSanePhpArray($library_label, $library_hours);
        $hours[$library_label] = $library_hours;
      }
    }

    return $hours;

  }

  /**
   * Call the ALMA API.
   *
   * @param string $library_code
   *   The code of the library for which to retrieve data.
   * @param string $endpoint
   *   The API endpoint to retrieve data from.
   * @param array $parameters
   *   Parameters for the url.
   *
   * @return bool|string|void
   *   The result of the REST API call.
   */
  protected function makeApiCall(string $library_code, string $endpoint, array $parameters) {
    $config = $this->config('alma_api.settings');
    $base_endpoint = $config->get('base_url') . '/' . $library_code . "/" . $endpoint;

    $query = [
      'apikey' => $config->get('api_key'),
      'format' => 'json',
    ] + $parameters;

    $request_url = Url::fromUri($base_endpoint, ['query' => $query])->toString();
    try {
      $response = $this->httpClient->get($request_url);
    }
    catch (RequestException $request_exception) {
      $message = $request_exception->getMessage();

      // Log error message.
      $context = [
        '@message' => $message,
      ];

      $this->logger->error($message, $context);

      $request_url = str_replace($query['api_key'], '***********', $request_url);
      $message = str_replace($query['api_key'], '***********', $message);
      $this->sendRequestFailureEmail($library_code, $parameters, $request_url, $message);

      return FALSE;
    }

    return $response->getBody()->getContents();

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'alma_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function configFactory() {
    if (!$this->configFactory) {
      $this->configFactory = $this->container->get('config.factory');
    }
    return $this->configFactory;
  }

  /**
   * Converts the hours json returned from the API to an PHP array.
   */
  protected function hoursFromJsonToSanePhpArray($library_label, $library_hours) {
    $library_hours = json_decode($library_hours);
    $hours = [];
    foreach ($library_hours->day as $day) {
      if (!isset($hours[$library_label])) {
        $hours[$library_label] = [];
      }
      $day->hour = (array) $day->hour;
      if (isset($day->hour[0])) {
        $hour = $day->hour[0];
        $date = $day->date;
        $hours[$library_label][$date] = ['hour' => $hour];
      }
    }
    return $hours;
  }

  /**
   * @param string $library_code
   * @param array $parameters
   * @param string $request_url
   * @param string $message
   */
  private function sendRequestFailureEmail(string $library_code, array $parameters, string $request_url, $message): void {

    $system_settings = $this->configFactory->get('system.site');
    $alma_api_settings = $this->configFactory->get('alma_api.settings');
    if ($alma_api_settings->get('failure_alert_email_enabled')) {

      $to = $system_settings->get('mail');
      if ($alma_api_settings->get('failure_alert_email_use_custom_address')) {
        $to = $alma_api_settings->get('failure_alert_email_custom_address');
      }

      $params['library'] = urldecode($library_code);

      $params['from'] = $parameters['from'];
      $params['to'] = $parameters['to'];
      $lang_code = $this->languageManager->getDefaultLanguage();

      $this->mailManager->mail('alma_api', 'opening_hours_alma_api_failure', $to, $lang_code, $params, NULL, TRUE);

    }

  }

}
