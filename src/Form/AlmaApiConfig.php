<?php

namespace Drupal\alma_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form for showing migration status.
 */
class AlmaApiConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alma_api_configuration_form';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'alma_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('alma_api.settings');
    $system_site_config = $this->config('system.site');

    $form = parent::buildForm($form, $form_state);

    $form['api_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
    ];
    $form['base_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Base URL'),
      '#default_value' => $config->get('base_url'),
    ];

    $form['failure_alert_email_enabled'] = [
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#title' => $this->t('Send email alert on API failure'),
      '#default_value' => $config->get('failure_alert_email_enabled'),
      '#description' => $this->t('Check this to enable sending an email when this site has a failure talking to the Alma API endpoint.'),
    ];

    $form['alert_email_settings'] = [
      '#type' => 'fieldset',
      '#title' => 'Failure email alert settings',
      '#states' => [
        'visible' => [
          '#edit-failure-alert-email-enabled' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['alert_email_settings']['failure_alert_email_use_custom_address'] = [
      '#type' => 'checkbox',
      '#required' => FALSE,
      '#title' => $this->t('Send to custom email address'),
      '#default_value' => $config->get('failure_alert_email_use_custom_address'),
      '#description' => $this->t('Uncheck this to use the site email address: @siteemail', [
        '@siteemail' => $system_site_config->get('mail'),
      ]),
    ];

    $form['alert_email_settings']['failure_alert_email_custom_address'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#title' => $this->t('Custom email address'),
      '#default_value' => $config->get('failure_alert_email_custom_address'),
      '#description' => $this->t('The custom email address to send alerts to.'),
      '#states' => [
        'visible' => [
          '#edit-failure-alert-email-use-custom-address' => ['checked' => TRUE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('alma_api.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('failure_alert_email_enabled', $form_state->getValue('failure_alert_email_enabled'))
      ->set('failure_alert_email_use_custom_address', $form_state->getValue('failure_alert_email_use_custom_address'))
      ->set('failure_alert_email_custom_address', $form_state->getValue('failure_alert_email_custom_address'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
