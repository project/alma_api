<?php

namespace Drupal\alma_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManager;
use GuzzleHttp\ClientInterface;

/**
 * Class Alma wrapper for the almas REST endpoint.
 *
 * @package Drupal\alma
 */
interface AlmaApiInterface {

  /**
   * AlmaApi constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   * @param \GuzzleHttp\ClientInterface $httpClient
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   * @param \Drupal\Core\Mail\MailManager $mailManager
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   */
  public function __construct(ConfigFactoryInterface $configFactory, ClientInterface $httpClient, LoggerChannelFactoryInterface $loggerFactory, MailManager $mailManager, LanguageManager $languageManager);

  /**
   * @param array $libraries
   *   Array of library codes to library names, like so:
   *   @code
   *   protected $libraries = [
   *     'LIB1' => 'Library 1',
   *     'LIB2' => 'Library 2',
   *     'LIB3' => 'Library 3',
   *     'LIB4' => 'Library 4',
   *     'LIB5' => 'Library 5',
   *   ];
   * @endcode
   */
  public function setLibraries(array $libraries);

  /**
   * Returns opening hours for all libraries set by setLibraries().
   *
   * @param string $from
   *   The date of format Y-m-d we want to retrieve hours from.
   * @param string $to
   *   The date of format Y-m-d we want to retrieve hours to.
   *
   * @return bool|string
   *   JSON response from API.
   */
  public function getLibraryHours(string $from, string $to);

}
